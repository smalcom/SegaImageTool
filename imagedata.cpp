#include "imagedata.hpp"

SPalette_SegaMD::SPalette_SegaMD()
	: Filled(false)
{
}

void SPalette_SegaMD::Clear()
{
	Filled = false;
	memset(ARGB, 0, sizeof(ARGB));
}

QRgb SPalette_SegaMD::QRGB_FromARGB(const QRgb pARGB)
{
	return (pARGB & 0x00E0E0E0) | 0xFF000000;// Ограничиваем диапазон и делаем цвет непрозрачным.
}

QRgb SPalette_SegaMD::QRGB_ExpTo_ARGB(const QRgb pSegaARGB)
{
QRgb tcol(pSegaARGB);

	// Добавляем младшие биты.
	if(tcol & 0x00E00000) tcol |= 0x001F0000;
	if(tcol & 0x0000E000) tcol |= 0x00001F00;
	if(tcol & 0x000000E0) tcol |= 0x0000001F;

	return tcol;
}

size_t ColorDistance(const QRgb pColor1, const QRgb pColor2)
{
size_t distance;
size_t tval;

	// R
	tval = labs(qRed(pColor1) - qRed(pColor2));
	distance = 30 * tval * tval;
	// G
	tval = labs(qGreen(pColor1) - qGreen(pColor2));
	distance += 59 * tval * tval;
	// B
	tval = labs(qBlue(pColor1) - qBlue(pColor2));
	distance += 11 * tval * tval;

	return distance;
}
