#pragma once

// Заголовчные файлы Qt.
#include <QLabel>
#include <QPixmap>

// Заголовчные файлы проекта
#include "imagedata.hpp"

class CPixmapSpace : public QLabel
{
	Q_OBJECT

private:

	const QColor ContourColor_Selected = QColorConstants::Green;///< Цвет выделенного контура.
	const QColor ContourColor_Unselected = QColorConstants::DarkBlue;///< Цвет невыделенного контура.
	const SSrcImageData& mSrcImageData;///< Исходные данные изображения.
	QPixmap mUI_Pixmap;///< Отображаемое изображение.

protected:

	virtual void paintEvent(QPaintEvent *pEvent);
	virtual int width() const;
	virtual int height() const;

public:

	CPixmapSpace(QWidget* pParent, const SSrcImageData& pSrcImageData);
	void ImageUpdated();
};
