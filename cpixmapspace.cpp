#include "cpixmapspace.hpp"

// Заголовочные файлы Qt.
#include <QPainter>
#include <QPaintEvent>

bool SRect::Contains(const SPoint pPoint)
{
	return ((pPoint.X >= TopLeft.X) && (pPoint.X <= BottomRight.X) && (pPoint.Y >= TopLeft.Y) && (pPoint.Y <= BottomRight.Y));
}

void CPixmapSpace::paintEvent(QPaintEvent *pEvent)
{
QPainter painter;

	if(!mSrcImageData.Image.isNull())
	{
		mUI_Pixmap = QPixmap::fromImage(mSrcImageData.Image);

		painter.begin(&mUI_Pixmap);
		// Начинаем отрисовку контуров в обычном цвете.
		painter.setPen(ContourColor_Unselected);
		for(size_t idx = 0, idx_e = mSrcImageData.Contour.size(); idx < idx_e; idx++)
		{
			const SRect& rect = mSrcImageData.Contour[idx];

			painter.drawRect(rect.TopLeft.X, rect.TopLeft.Y, rect.BottomRight.X - rect.TopLeft.X, rect.BottomRight.Y - rect.TopLeft.Y);
		}

		// Отрисовка выделенных контуров.
		painter.setPen(ContourColor_Selected);
		for(size_t idx = 0, idx_e = mSrcImageData.ContourSelected.size(); idx < idx_e; idx++)
		{
			const SRect& rect = mSrcImageData.Contour[mSrcImageData.ContourSelected[idx]];

			painter.drawRect(rect.TopLeft.X, rect.TopLeft.Y, rect.BottomRight.X - rect.TopLeft.X, rect.BottomRight.Y - rect.TopLeft.Y);
		}

		painter.end();

		// Отображаем изображение.
		painter.begin(this);
		painter.drawPixmap(mSrcImageData.Image.rect(), mUI_Pixmap);
		painter.end();
	}

	pEvent->accept();
}

int CPixmapSpace::width() const
{
	return mSrcImageData.Image.width();
}

int CPixmapSpace::height() const
{
	return mSrcImageData.Image.height();
}

CPixmapSpace::CPixmapSpace(QWidget *pParent, const SSrcImageData& pSrcImageData)
	: QLabel(pParent), mSrcImageData(pSrcImageData)
{
}

void CPixmapSpace::ImageUpdated()
{
	setFixedSize(mSrcImageData.Image.size());
}
