#include "savespritesheet.hpp"
#include "ui_savespritesheet.h"

// Заголовочные файлы Qt.
#include <QLabel>
#include <QPixmap>

SaveSpriteSheet::SaveSpriteSheet(QWidget *pParent, const SSrcImageData& pSrcImageData, const SPalette_SegaMD& pTgtPalette)
	: QDialog(pParent), mSrcImageData(pSrcImageData), mTgtPalette(pTgtPalette), ui(new Ui::SaveSpriteSheet)
{
/// \brief Возвращает индекс цвета. Заполняет карту соответствия цветов.
auto GetIndexedColor = [&](const QRgb& pSrcColor) -> size_t
	{
		QRgb tgt_col = SPalette_SegaMD::QRGB_FromARGB(pSrcColor);

		if(!mTgtImageData.ColorMap.contains(pSrcColor))
		{// Если цвета нет в карте, то ищем в палитре ближайший. При этом работаем с целевым форматом цветов.
			size_t min_distance = SIZE_MAX;
			size_t min_idx;

			// Первым делом проверяем альфа-канал. Если цвет прозрачный, то это всегда будет интерпретироваться как фон.
			if((qAlpha(pSrcColor) & 0x000000FF) == 0x000000FF)
			{
				for(size_t cidx = 0; cidx < SPalette_SegaMD::Color_MaxCount; cidx++)
				{
					size_t tval;

					tval = ColorDistance(mTgtPalette.ARGB[cidx], tgt_col);
					if(tval < min_distance)
					{
						min_distance = tval;
						min_idx = cidx;
					}
				}
			}
			else
			{
				min_idx = 0;
			}

			mTgtImageData.ColorMap.insert(pSrcColor, min_idx);
		}

		// Цвет либо имеется в карте, либо только что был добавлен.
		return mTgtImageData.ColorMap.value(pSrcColor);
	};

	ui->setupUi(this);
	//
	// Создаём изображение, состоящее из выделенных частей исходного изображения.
	//
	// 1. Целевое и изображение предпросмотра будут использовать индексированный цвет. Палитра для целевого изображения уже есть, а для предпросмотра нет - создаём её.
	mTgtImageData.ColorTable.resize(SPalette_SegaMD::Color_MaxCount);
	for(size_t ci = 0; ci < SPalette_SegaMD::Color_MaxCount; ci++)
		mTgtImageData.ColorTable[ci] = SPalette_SegaMD::QRGB_ExpTo_ARGB(mTgtPalette.ARGB[ci]);

	// Обход выделенных контуров.
	mTgtImageData.ImageData.resize(mSrcImageData.ContourSelected.size());
	for(size_t idx_sc = 0, idx_sc_e = mSrcImageData.ContourSelected.size(); idx_sc < idx_sc_e; idx_sc++)
	{
		// 2. Получаем размер выделенной части.
		const SRect& rect_src = mSrcImageData.Contour[mSrcImageData.ContourSelected[idx_sc]];

		QImage* timg;
		QLabel* lbl;
		QSize size_tgt(rect_src.BottomRight.X - rect_src.TopLeft.X + 1, rect_src.BottomRight.Y - rect_src.TopLeft.Y + 1);
		size_t tval;

		// 3. Вычисляем размер части целевого изображения.
		tval = size_tgt.width() % 8;
		if(tval != 0) size_tgt.rwidth() += 8 - tval;

		tval = size_tgt.height() % 8;
		if(tval != 0) size_tgt.rheight() += 8 - tval;

		// 4. Создаём изображение с индексированной палитрой.
		timg = new QImage(size_tgt, QImage::Format_Indexed8);
		timg->setColorTable(mTgtImageData.ColorTable);
		timg->fill(0);
		// 5. Заполняем изображение выделенной частью исходного изображения и попутно заполняем карту соответствий цветов.
		// Прежде чем будем заполнять карту, сразу зададим цвет фона, т. к. цвета могут находится на большой дистанции. Это связано с тем, что на целевой платформе цвет с индексом
		// 0 является полность прозрачным, хотя и может иметь компоненты RGB. Как и ранее цвет фона возьмём в точке (0, 0).
		mTgtImageData.ColorMap.insert(mSrcImageData.Image.pixel(0, 0), 0);
		// Вот теперь можно добавлять остальные цвета.
		for(size_t y_c = rect_src.TopLeft.Y, y_c_e = rect_src.BottomRight.Y, tgt_y = 0; y_c <= y_c_e; y_c++, tgt_y++)
		{
			for(size_t x_c = rect_src.TopLeft.X, x_c_e = rect_src.BottomRight.X, tgt_x = 0; x_c <= x_c_e; x_c++, tgt_x++)
				timg->setPixel(tgt_x, tgt_y, GetIndexedColor(mSrcImageData.Image.pixel(x_c, y_c)));
		}

		// 5. Создаём метку
		mTgtImageData.ImageData[idx_sc].Image = timg;
		mTgtImageData.ImageData[idx_sc].Pixmap = QPixmap::fromImage(*timg);
		lbl = new QLabel(ui->scrollArea);
		lbl->setAutoFillBackground(true);
		lbl->setFixedSize(timg->size());
		lbl->setPixmap(mTgtImageData.ImageData[idx_sc].Pixmap);
		ui->layScroll->addWidget(lbl);
	}// for(size_t idx_sc = 0, idx_sc_e = mSrcImageData.ContourSelected.size(); idx_sc < idx_sc_e; idx_sc++)
}

SaveSpriteSheet::~SaveSpriteSheet()
{
	foreach(STgtImageData::SImageData imgdata, mTgtImageData.ImageData)
	{
		delete imgdata.Image;
	}

	delete ui;
}

const QImage* SaveSpriteSheet::Result_Image(const size_t pImageIdx) const
{
	if(pImageIdx < (size_t)mTgtImageData.ImageData.size())
		return mTgtImageData.ImageData[pImageIdx].Image;
	else
		return nullptr;
}

size_t SaveSpriteSheet::Result_ImageCount() const
{
	return mTgtImageData.ImageData.size();
}

QString SaveSpriteSheet::Result_NameBase() const
{
	return ui->leNameBase->text();
}

bool SaveSpriteSheet::Result_TileOrder_Sprite() const
{
	return ui->rbtnTileOrder_Sprite->isChecked();
}

bool SaveSpriteSheet::Result_UniteFiles() const
{
	return ui->chkUnite->isChecked();
}
