#include "progress.hpp"
#include "ui_progress.h"

Progress::Progress(QWidget *pParent)
	: QDialog(pParent), ui(new Ui::Progress)
{
	ui->setupUi(this);
}

Progress::~Progress()
{
	delete ui;
}

void Progress::SetTitle(const QString& pTitle)
{
	setWindowTitle(pTitle);
	QCoreApplication::processEvents();
}

void Progress::SetProgress(const size_t pPercent)
{
	ui->pbarProgress->setValue(pPercent <= 100 ? pPercent : 100);
	QCoreApplication::processEvents();
}
