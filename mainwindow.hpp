#pragma once

// Заголвочные файлы Qt.
#include <QFile>
#include <QMainWindow>
#include <QRgb>

// Заголовочные файлы проекта.
#include "cpixmapspace.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

private:

	static constexpr size_t Contour_SeekDistance = 8;///< Ширина области для проверки наличия точек.
	const QString DlgCap_Error = tr("Ошибка");///< Заголовок сообщения об ошибке.
	const QString DlgTitle_FileOpenError = tr("Ошибка открытия файла: %1");///< Текст сообщения об ошибке.
	const QString DlgTitle_FileWriteError = tr("Ошибка записи в файл: %1");///< Текст сообщения об ошибке.

	Ui::MainWindow *ui;
	QImage mUI_PaletteImage;///< Изображение для визуализации палитры.
	SSrcImageData mSrcImageData;///< Данные исходного изображения.
	SPalette_SegaMD mPalette_SegaMD;///< Палитра изображения для SegaMD.
	bool mBuildMode;///< Режим построение спрайт-листа.

	/// \brief Обновляет изображение палитры, основываясь на \ref mPalette_SegaMD.
	void UI_UpdatePalette();

	/// \brief Сохраняет в файлы плитки и прииер использования.
	/// \return true - если данные успешно записаны, иначе - false.
	bool SaveImage(const QImage* const pImage, const size_t pImageIndex, const bool pTileOrder_Sprite, QFile& pFile_Tile, QFile& pFile_Desc);

	/// \brief Определяет находится точка в одном из контуров или нет. ( \sa SSrcImageData::Contour ).
	/// \param [out] pContourIndex - если не равен nullptr, то будет возвращён индекс контура, в котором находится точка.
	bool PointInContour(const size_t pX, const size_t pY, size_t* const pContourIndex = nullptr);

	/// \brief Создаёт контур, основываясь на стартовой точке.
	/// \return прямоугольник, описывающий созданный контур.
	SRect CreateContour(const size_t pX, const size_t pY);

	/// \brief Выполняет поиск точек в области вокруг указанной.
	/// \details Функция проверяет наличие точек в области вокруг указанного прямоугольника, но не выше его верхней границы. Для примера введём некоторые обозначения:
	/// # S - область без точек, в которой находится только фон;
	/// # U - область с неизвестным содержанием;
	/// # T - проверяемая область;
	/// # R - заданный прямоугольник ( \ref pRectangleContour).
	/// Пусть задана следующая область:
	/// SSSSSSSSSSSS
	/// SSSSSRRUUUUU
	/// UUUUURRUUUUU
	/// UUUUUUUUUUUU
	/// UUUUUUUUUUUU
	/// UUUUUUUUUUUU
	/// Тогда функция выполнит следующую проверку (при pDistance = 2):
	/// SSSSSSSSSSSS
	/// SSSTTRRTTUUU
	/// UUUTTRRTTUUU
	/// UUUTTTTTTUUU
	/// UUUTTTTTTUUU
	/// UUUUUUUUUUUU
	/// \param [in, out] pTopLeft - Левая верхняя точка прямоугольника лежащего на крайних точках.
	/// \param [in, out] pBottomRight - Правая нижняя точка прямоугольника лежащего на крайних точках.
	/// \param [in] pDistance - ширина проверяемой области.
	/// \return true - если были найдены новые точки и pRectangleContour изменён, иначе - false.
	bool SeekPointsInRect(SPoint& pTopLeft, SPoint& pBottomRight, const size_t pDistance);

	/// \brief Включить режим построения спрайт-листа.
	void BuildMode_Enable();

	/// \brief Выключить режим построения спрайт-листа и отменить выделение.
	void BuildMode_Cancel();

	/// \brief Выключить режим построения спрайт-листа и перейти к сохранению выделенных контуров.
	void BuildMode_Finish();

private slots:

	void on_btnOpenImage_clicked();
	void on_btnOpenPalette_clicked();
	void on_btnCreatePalette_clicked();
	void on_btnCreateSpriteSheet_clicked(bool pChecked);

protected:

	virtual void mousePressEvent(QMouseEvent *pEvent);
	virtual void keyPressEvent(QKeyEvent *pEvent);

public:

	MainWindow(QWidget *pParent = nullptr);
	~MainWindow();
};
