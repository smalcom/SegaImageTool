#include "mainwindow.hpp"
#include "ui_mainwindow.h"

// Заголовочные файлы Qt.
#include <QDialog>
#include <QFileDialog>
#include <QKeyEvent>
#include <QMessageBox>
#include <QMouseEvent>
#include <QVector>

// Заголовочные файлы стандартной библиотеки.
#include <math.h>

// Заголовочные файлы проекта.
#include "progress.hpp"
#include "savespritesheet.hpp"

void MainWindow::UI_UpdatePalette()
{
	if(mPalette_SegaMD.Filled)
	{
		// Сначала задаём нужные цвета точкам изображения,
		for(size_t col_idx = 0; col_idx < SPalette_SegaMD::Color_MaxCount; col_idx++)
			mUI_PaletteImage.setPixel(col_idx, 0, SPalette_SegaMD::QRGB_ExpTo_ARGB(mPalette_SegaMD.ARGB[col_idx]));

		// а затем устанавливаем изображение на виджет.
		ui->lblPalette->setPixmap(QPixmap::fromImage(mUI_PaletteImage.scaled(ui->lblPalette->size())));
	}
	else
	{// Если палитра не задана, то очищаем её область вывода.
		ui->lblPalette->clear();
	}
}

bool MainWindow::SaveImage(const QImage* const pImage, const size_t pImageIndex, const bool pTileOrder_Sprite, QFile& pFile_Tile, QFile& pFile_Desc)
{
size_t x_img = 0, y_img = 0;
QString desc;

	// Сохраняем плитки.
	if(pTileOrder_Sprite)
	{
		desc = QString(
					"// %1\n"
					"VDP_loadTileData((const u32*)tile_data, tile_idx, %2 /* tile_count */, CPU /* DMA */);\n"
					"VDP_setSpriteFull(spr_idx, x, y, SPRITE_SIZE(w /* %3 */, h /* %4 */), TILE_ATTR_FULL(palette, priority, flip_v, flip_h, tile_idx), spr_idx + 1);\n\n"
				).arg(pImageIndex)
					.arg((pImage->width() / 8) * (pImage->height() / 8))
					.arg(pImage->width())
					.arg(pImage->height());

		do// Копирование последовательности столбцов.
		{
			y_img = 0;
			do// Копирование столбца плиток.
			{
				// Копирование данных текущей плитки.
				for(size_t x_tile = x_img, x_tile_e = x_img + 8; x_tile < x_tile_e; x_tile++)
				{
					for(size_t y_tile = y_img, y_tile_e = y_img + 8; y_tile < y_tile_e; y_tile += 2)
					{
						uint8_t tile;

						tile = (pImage->pixelIndex(x_tile, y_tile) << 4) | pImage->pixelIndex(x_tile, y_tile + 1);
						if(pFile_Tile.write((const char*)&tile, 1) != 1)
						{
							QMessageBox::critical(this, DlgCap_Error, DlgTitle_FileWriteError.arg(pFile_Tile.fileName()));

							return false;
						}
					}// for(size_t y_tile = y_img, y_tile_e = y_img + 8; y_tile < y_tile_e; y_tile += 2)
				}// for(size_t x_tile = x_img, x_tile_e = x_img + 8; x_tile < x_tile_e; x_tile++)

				y_img += 8;// Переходим к плитке снизу.
			} while(y_img < (size_t)pImage->height());

			x_img += 8;// Переходим к следующему столбцу плиток.
		} while(x_img < (size_t)pImage->width());
	}// if(pTileOrder_Sprite)
	else
	{
		desc = QString(
					"// %1\n"
					"VDP_loadTileData((const u32*)tile_data, tile_idx, %2 /* tile_count */, CPU /* DMA */);\n"
					"VDP_fillTileMapRectInc(plane, TILE_ATTR_FULL(palette, priority, flip_v, flip_h, tile_idx), x, y, %3 /* width */, %4 /* height */);\n\n"
				).arg(pImageIndex)
					.arg((pImage->width() / 8) * (pImage->height() / 8))
					.arg(pImage->width())
					.arg(pImage->height());

		do// Копирование последовательности строк.
		{
			x_img = 0;
			do// Копирование строки плиток.
			{
				// Копирование данных текущей плитки.
				for(size_t y_tile = y_img, y_tile_e = y_img + 8; y_tile < y_tile_e; y_tile++)
				{
					for(size_t x_tile = x_img, x_tile_e = x_img + 8; x_tile < x_tile_e; x_tile += 2)
					{
						uint8_t tile;

						tile = (pImage->pixelIndex(x_tile, y_tile) << 4) | pImage->pixelIndex(x_tile + 1, y_tile);
						if(pFile_Tile.write((const char*)&tile, 1) != 1)
						{
							QMessageBox::critical(this, DlgCap_Error, DlgTitle_FileWriteError.arg(pFile_Tile.fileName()));

							return false;
						}
					}// for(size_t x_tile = x_img, x_e = x_img + 8; x < x_e; x_tile += 2)
				}// for(size_t y_tile = y_img, y_tile_e = pImage.height(); y_tile < y_tile_e; y_tile++)

				x_img += 8;// Переходим к плитке справа.
			} while(x_img < (size_t)pImage->width());

			y_img += 8;// Переходим к следующей строке плиток.
		} while(y_img < (size_t)pImage->height());
	}// if(pTileOrder_Sprite)

	// Сохраняем пример использования.
	if(pFile_Desc.write(desc.toLocal8Bit().data()) != desc.toLocal8Bit().size())
	{
		QMessageBox::critical(this, DlgCap_Error, DlgTitle_FileWriteError.arg(pFile_Tile.fileName()));

		return false;
	}

	return true;
}

bool MainWindow::PointInContour(const size_t pX, const size_t pY, size_t* const pContourIndex)
{
	for(size_t idx = 0, idx_e = mSrcImageData.Contour.size(); idx < idx_e; idx++)
	{
		if(mSrcImageData.Contour[idx].Contains(SPoint(pX, pY)))
		{
			if(pContourIndex != nullptr)
				*pContourIndex = idx;

			return true;
		}
	}

	return false;
}

SRect MainWindow::CreateContour(const size_t pX, const size_t pY)
{
SPoint topleft(pX, pY);
SPoint botright(pX, pY);

	// Проверяем область вокруг прямоугольника пока есть точки.
	do {} while(SeekPointsInRect(topleft, botright, Contour_SeekDistance));

	return SRect(topleft, botright);
}

bool MainWindow::SeekPointsInRect(SPoint& pTopLeft, SPoint& pBottomRight, const size_t pDistance)
{
auto CheckRect = [&](SRect& pRect_Test, SRect& pRect_Points) -> void
	{
		for(size_t y = pRect_Test.TopLeft.Y, y_l = pRect_Test.BottomRight.Y; y <= y_l; y++)
		{
			for(size_t x = pRect_Test.TopLeft.X, x_l = pRect_Test.BottomRight.X; x <= x_l; x++)
			{
				if(SPalette_SegaMD::QRGB_FromARGB(mSrcImageData.Image.pixel(x, y)) != mSrcImageData.Palette_SegaMD[0].TgtARGB)
				{// Найдена точка, проверим её координаты.
					if(x < pRect_Points.TopLeft.X)
						pRect_Points.TopLeft.X = x;
					else if(x > pRect_Points.BottomRight.X)
						pRect_Points.BottomRight.X = x;

					if(y > pRect_Points.BottomRight.Y)
						pRect_Points.BottomRight.Y = y;
				}
			}// for(size_t x = pRect_Test.TopLeft.X, x_l = pRect_Test.BottomRight.X; x <= x_l; x++)
		}
	};

SRect rect_test, rect_points;// Проверяемая область и обнаруженные крайние точки.
SPoint tl_res(pTopLeft), br_res(pBottomRight);// Новые значения, расширяющие прямоугольник основанный на pTopLeft и pBottomRight.
bool found = false;

	// В данной функции мы ещё раз проверим наличие точкм в pTopLeft, хотя и знаем, что она там есть. Так мы опустим несколько условий при вызове проверяющих функции и оставим
	// только пару проверок уже в конце функции.
	//
	// Левая колонка.
	//
	// На основе примера: L - проверяемая колонка.
	/// SSSSSSSSSSSS
	/// SSSLLRRUUUUU
	/// UUULLRRUUUUU
	/// UUULLUUUUUUU
	/// UUULLUUUUUUU
	/// UUUUUUUUUUUU
	if(pTopLeft.X > 0)
	{
		// Задаём область проверки.
		rect_test.TopLeft.X = (pTopLeft.X > pDistance ? pTopLeft.X - pDistance : 0);
		rect_test.TopLeft.Y = pTopLeft.Y;
		rect_test.BottomRight.X = pTopLeft.X - 1;
		rect_test.BottomRight.Y = pBottomRight.Y + pDistance;
		if(rect_test.BottomRight.Y >= (size_t)mSrcImageData.Image.height()) rect_test.BottomRight.Y = mSrcImageData.Image.height() - 1;

		// Задаём начальные крайние точки.
		rect_points.TopLeft = pTopLeft;
		rect_points.BottomRight = pTopLeft;
		// Поиск точек и проверка найденного.
		CheckRect(rect_test, rect_points);
		if(rect_points.TopLeft.X < tl_res.X) { tl_res.X = rect_points.TopLeft.X; found = true; }
		if(rect_points.BottomRight.Y > br_res.Y) { br_res.Y = rect_points.BottomRight.Y; found = true; }

	}

	//
	// Нижняя строка.
	//
	// На основе примера: D - проверяемая строка.
	/// SSSSSSSSSSSS
	/// SSSUURRUUUUU
	/// UUUUURRUUUUU
	/// UUUUUDDUUUUU
	/// UUUUUDDUUUUU
	/// UUUUUUUUUUUU
	if(pBottomRight.Y < ((size_t)mSrcImageData.Image.height() - 1))
	{
		// Задаём область проверки.
		rect_test.TopLeft.X = pTopLeft.X;
		rect_test.TopLeft.Y = pBottomRight.Y + 1;
		rect_test.BottomRight.X = pBottomRight.X;
		rect_test.BottomRight.Y = pBottomRight.Y + pDistance;
		if(rect_test.BottomRight.Y >= (size_t)mSrcImageData.Image.height()) rect_test.BottomRight.Y = mSrcImageData.Image.height() - 1;

		// Задаём начальные крайние точки.
		rect_points.TopLeft = pTopLeft;
		rect_points.BottomRight = pBottomRight;
		// Поиск точек и проверка найденного.
		CheckRect(rect_test, rect_points);
		if(rect_points.BottomRight.Y > br_res.Y) { br_res.Y = rect_points.BottomRight.Y; found = true; }

	}

	//
	// Правая колонка.
	//
	// На основе примера: C - проверяемая колонка.
	/// SSSSSSSSSSSS
	/// SSSUURRCCUUU
	/// UUUUURRCCUUU
	/// UUUUUUUCCUUU
	/// UUUUUUUCCUUU
	/// UUUUUUUUUUUU
	if(pBottomRight.X < ((size_t)mSrcImageData.Image.width() - 1))
	{
		// Задаём область проверки.
		rect_test.TopLeft.X = pBottomRight.X + 1;
		rect_test.TopLeft.Y = pTopLeft.Y;
		rect_test.BottomRight.X = pBottomRight.X + pDistance;
		rect_test.BottomRight.Y = pBottomRight.Y + pDistance;
		if(rect_test.BottomRight.X >= (size_t)mSrcImageData.Image.width()) rect_test.BottomRight.X = mSrcImageData.Image.width() - 1;
		if(rect_test.BottomRight.Y >= (size_t)mSrcImageData.Image.height()) rect_test.BottomRight.Y = mSrcImageData.Image.height() - 1;

		// Задаём начальные крайние точки.
		rect_points.TopLeft.X = pBottomRight.X;
		rect_points.TopLeft.Y = pTopLeft.Y;
		rect_points.BottomRight = pBottomRight;
		// Поиск точек и проверка найденного.
		CheckRect(rect_test, rect_points);
		if(rect_points.BottomRight.X > br_res.X) { br_res.X = rect_points.BottomRight.X; found = true; }
		if(rect_points.BottomRight.Y > br_res.Y) { br_res.Y = rect_points.BottomRight.Y; found = true; }

	}

	//
	// Проверка вычислений.
	//
	if(found)
	{
		found = false;
		if(tl_res.X < pTopLeft.X) { pTopLeft.X = tl_res.X; found = true; }
		if(br_res.X > pBottomRight.X) { pBottomRight.X = br_res.X; found = true; }
		if(br_res.Y > pBottomRight.Y) { pBottomRight.Y = br_res.Y; found = true; }

	}

	return found;
}

void MainWindow::BuildMode_Enable()
{
	mBuildMode = true;
	mSrcImageData.ContourSelected.clear();
	ui->scrollArea->widget()->update();
	// Отключаем кнопки, для которых не время.
	ui->btnOpenImage->setEnabled(false);
	ui->btnOpenPalette->setEnabled(false);
	ui->btnCreatePalette->setEnabled(false);
}

void MainWindow::BuildMode_Cancel()
{
	mBuildMode = false;
	mSrcImageData.ContourSelected.clear();
	ui->scrollArea->widget()->update();
	ui->btnCreateSpriteSheet->setChecked(false);// Отжимаем кнопку создания спрайт-листа.
	// Возвращаем к жизни отключенные кнопки.
	ui->btnOpenImage->setEnabled(true);
	ui->btnOpenPalette->setEnabled(true);
	ui->btnCreatePalette->setEnabled(true);
}

void MainWindow::BuildMode_Finish()
{
SaveSpriteSheet dlg_spritesheet(this, mSrcImageData, mPalette_SegaMD);

auto CreateOrUseFile = [&](QFile& pFile_Image, QFile& pFile_Desc, const QString& pFileName, const bool pUnite, const size_t pIndex) -> bool
	{
		if(pUnite)
		{
			pFile_Image.setFileName(pFileName + ".bin");
			pFile_Desc.setFileName(pFileName + ".dsc");
		}
		else
		{
			// Закрываем открытые файлы.
			if(pFile_Image.isOpen()) pFile_Image.close();
			if(pFile_Desc.isOpen()) pFile_Desc.close();

			// Задаём новое имя файла, который будет открыт ниже.
			pFile_Image.setFileName(pFileName + QString("_%1.bin").arg(pIndex));
			pFile_Desc.setFileName(pFileName + QString("_%1.c").arg(pIndex));
		}

		// Открываем неоткрытые файлы. Нужные имена были установлены выше.
		if(!pFile_Image.isOpen())
		{
			if(!pFile_Image.open(QFile::WriteOnly | QFile::Truncate))
			{
				QMessageBox::critical(this, DlgCap_Error, DlgTitle_FileOpenError.arg(pFile_Image.fileName()));

				return false;
			}
		}

		if(!pFile_Desc.isOpen())
		{
			if(!pFile_Desc.open(QFile::WriteOnly | QFile::Truncate))
			{
				QMessageBox::critical(this, DlgCap_Error, DlgTitle_FileOpenError.arg(pFile_Desc.fileName()));

				return false;
			}
		}

		return true;
	};

int res;

	// Окно для просмотра сохраняемых данных.
	res = dlg_spritesheet.exec();
	if(res == QDialog::DialogCode::Accepted)
	{
		QString basename = QFileInfo(mSrcImageData.FileName).path() + dlg_spritesheet.Result_NameBase();

		if(dlg_spritesheet.Result_ImageCount() > 0)
		{
			QFile file_img, file_desc;

			for(size_t idx_im = 0, idx_im_e = dlg_spritesheet.Result_ImageCount(); idx_im < idx_im_e; idx_im++)
			{
				if(!CreateOrUseFile(file_img, file_desc, basename, dlg_spritesheet.Result_UniteFiles(), idx_im))
					break;

				if(!SaveImage(dlg_spritesheet.Result_Image(idx_im), idx_im, dlg_spritesheet.Result_TileOrder_Sprite(), file_img, file_desc))
					break;
			}
			// Файлы будут закрыты в деструкторе при выходе из блока.
		}
	}

	BuildMode_Cancel();// Отменяем режим выделения.
}

void MainWindow::mousePressEvent(QMouseEvent *pEvent)
{
	// Нажатие кнопок обрабатываем только в режиме построения спрайт-листа.
	if(mBuildMode)
	{
		QWidget* child;
		QPoint butcoord;

		// Т. к. обработчик нажатий глобальный, а не для виджета, то необходимо сделать две операции:
		// - убедиться, что курсор показывает на виджет с изображением;
		// - преобразовать координаты из координатной системы главного окна в координатную систему изображения.
		butcoord = pEvent->pos();
		child = childAt(butcoord);
		if(child == ui->scrollArea->widget())
		{
			if(pEvent->button() == Qt::LeftButton)
			{
				QPoint imgcoord;
				size_t cidx;

				imgcoord = ui->scrollArea->widget()->mapFrom(this, butcoord);
				if(PointInContour(imgcoord.x(), imgcoord.y(), &cidx))
				{
					// Добавим контур в выделенные и обновим изображение.
					mSrcImageData.ContourSelected.push_back(cidx);
					ui->scrollArea->widget()->update();
				}
			}
			else if(pEvent->button() == Qt::RightButton)
			{
				BuildMode_Finish();
			}
		}

		pEvent->accept();
	}
}

void MainWindow::keyPressEvent(QKeyEvent *pEvent)
{
	// Клавиши мы обрабатываем только в режиме построения спрайт-листа.
	if(!mBuildMode) return;

	if(pEvent->key() == Qt::Key_Escape)
	{
		BuildMode_Cancel();
		pEvent->accept();
	}
}

MainWindow::MainWindow(QWidget *pParent)
	: QMainWindow(pParent), ui(new Ui::MainWindow), mUI_PaletteImage(SPalette_SegaMD::Color_MaxCount, 1, QImage::Format_ARGB32), mBuildMode(false)
{
CPixmapSpace* lblPixmapSpace;

	ui->setupUi(this);
	// Добавляем метку для вывода изображения.
	lblPixmapSpace = new CPixmapSpace(ui->scrollArea, mSrcImageData);
	lblPixmapSpace->setObjectName(QString::fromUtf8("lblPixmapSpace"));
	lblPixmapSpace->setEnabled(true);
	lblPixmapSpace->setAutoFillBackground(true);
	lblPixmapSpace->setText("");
	ui->scrollArea->setWidget(lblPixmapSpace);
	ui->scrollArea->setWidgetResizable(false);
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_btnOpenImage_clicked()
{
auto FindColor = [&](const QRgb pColor, size_t& pIndex, QRgb* pTargetColor = nullptr) -> bool
{
	QRgb col_tgt = SPalette_SegaMD::QRGB_FromARGB(pColor);
	bool rv = false;// Цвет не найден.

	// Поиск цвета в палитре.
	for(size_t idx = 0, idx_e = mSrcImageData.Palette_SegaMD.size(); idx < idx_e; idx++)
	{
		if(mSrcImageData.Palette_SegaMD[idx].TgtARGB == col_tgt)
		{
			pIndex = idx;
			rv = true;// Цвет найден.

			break;
		}
	}

	if(pTargetColor != nullptr)
		*pTargetColor = col_tgt;

	return rv;
};

auto AddColor = [&](const QRgb pColor) -> void
	{
		QRgb col_tgt;
		size_t idx;

		if(FindColor(pColor, idx, &col_tgt))
			mSrcImageData.Palette_SegaMD[idx].Count++;// Цвет найден - увеличиваем число ссылок на него.
		else
			mSrcImageData.Palette_SegaMD.push_back({ .TgtARGB = col_tgt, .Count = 1, .TgtPal_ColIdx = 0 });// Цвет не найден: добавляем его и задаём начальное значение счётчика ссылок.
	};

Progress dlg_progress(this);

	mSrcImageData.FileName = QFileDialog::getOpenFileName(this, tr("Открыть изображение"), QDir::homePath(), "PNG (*.png);;JPEG (*.jpg *.jpeg);;Bitmap (*.bmp)");
	if(mSrcImageData.FileName.isEmpty()) return;

	if(!mSrcImageData.Image.load(mSrcImageData.FileName))
	{
		QMessageBox::critical(this, DlgCap_Error, tr("Не удалось открыть изображение."));
		mSrcImageData.FileName.clear();

		return;
	}

	// После того как изображение открыто, необходимо собрать данные о палитре.
	mSrcImageData.Palette_SegaMD.clear();
	// Для изображений, которые используют индексированный цвет, функция "QImage::pixel" будет возвращать цвет, а не индекс. Пожтому можно не выделять обработку таких изображений
	// в отдельный алгоритм.
	// Добавление цветов при прямом задании цвета. И подсчёт количества ссылок для обоих вариантов хранения цвета.
	dlg_progress.show();
	dlg_progress.SetTitle(tr("Извлечение цветов изображения"));
	for(size_t y = 0, y_e = mSrcImageData.Image.height(); y < y_e; y++)
	{
		dlg_progress.SetProgress(100 * y / y_e);
		for(size_t x = 0, x_e = mSrcImageData.Image.width(); x < x_e; x++)
			AddColor(mSrcImageData.Image.pixel(x, y));
	}

	{// Определяем цвет фона.
		QRgb col_backg = mSrcImageData.Image.pixel(0, 0);
		size_t idx_backg;

		if(FindColor(col_backg, idx_backg))
		{
			// Цвет фона должен быть в начале палитры.
			if(idx_backg != 0)
				mSrcImageData.Palette_SegaMD.swapItemsAt(0, idx_backg);
		}
	}

	// Сортировка по количеству ссылок. Выполняется для всех цветов кроме фона (первый цвет). Сортировка нужна, чтобы часто используемые цвета переместить в начало и по
	// виду палитры можно было определить какие цвета более важны. Всё это относится к палитре, которая создаётся на основе загруженного изображения.
	if(mSrcImageData.Palette_SegaMD.size() > 2)
	{
		auto BiggerThan = [&](const SSrcImageData::SPalette pValue1, const SSrcImageData::SPalette pValue2) -> bool { return pValue1.Count > pValue2.Count; };

		std::sort(mSrcImageData.Palette_SegaMD.begin() + 1, mSrcImageData.Palette_SegaMD.end(), BiggerThan);
	}

	{// Определение контуров.
		QRgb tcol;

		mSrcImageData.Contour.clear();
		BuildMode_Cancel();// Выключаем режим выделения, чтобы задать нужные значения переменным.
		dlg_progress.SetTitle(tr("Определение контуров"));
		for(size_t y = 0, y_e = mSrcImageData.Image.height(); y < y_e; y++)
		{
			dlg_progress.SetProgress(100 * y / y_e);
			for(size_t x = 1 /* 0 - мы уже используем как фон */, x_e = mSrcImageData.Image.width(); x < x_e; x++)
			{
				tcol = SPalette_SegaMD::QRGB_FromARGB(mSrcImageData.Image.pixel(x, y));
				if(tcol != mSrcImageData.Palette_SegaMD[0].TgtARGB)
				{// Найдена точка с цветом отличным от фона. Проверим, что она уже не добавлена в какой-то из контуров.
					if(!PointInContour(x, y))
						mSrcImageData.Contour.push_back(CreateContour(x, y));// Создаём новый контур.
				}
			}// for(size_t x = 1, x_e = mSrcImageData.Image.width(); x < x_e; x++)
		}// for(size_t y = 0, y_e = mSrcImageData.Image.height(); y < y_e; y++)
	}

	// Отображаем изображение.
	static_cast<CPixmapSpace*>(ui->scrollArea->widget())->ImageUpdated();
	ui->scrollArea->widget()->update();
	// Изображение открыто и теперь можно некоторые элементы управления.
	ui->btnCreateSpriteSheet->setEnabled(true);
	ui->btnCreatePalette->setEnabled(true);
}

void MainWindow::on_btnOpenPalette_clicked()
{
QString filename;
QFile file;

	filename = QFileDialog::getOpenFileName(this, tr("Открыть палитру"), QDir::homePath(), "BIN (*.bin)");
	if(filename.isEmpty()) return;

	file.setFileName(filename);
	if(!file.open(QIODevice::ReadOnly))
	{
		QMessageBox::critical(this, DlgCap_Error, tr("Не удалось открыть файл."));

		return;
	}

	// Подготовим целевую палитру и файл-источник.
	mPalette_SegaMD.Clear();
	file.seek(0);
	// Копируем цвета.
	{
		size_t idx_col;

		for(idx_col = 0; idx_col < SPalette_SegaMD::Color_MaxCount; idx_col++)
		{
			uint16_t sega_col;

			if(file.read((char*)&sega_col, SPalette_SegaMD::Color_Size) != SPalette_SegaMD::Color_Size)
				break;// Ошибка или файл закончился.

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
			mPalette_SegaMD.ARGB[idx_col] = qRgba((sega_col >> 4) & 0x000000E0, (sega_col >> 8) & 0x000000E0, (sega_col << 4) & 0x000000E0, 0xFF);
#else
			mPalette_SegaMD.ARGB[idx_col] = qRgba((sega_col << 4) & 0x000000E0, sega_col & 0x000000E0, (sega_col >> 4) & 0x000000E0, 0xFF);
#endif // __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
		}

		mPalette_SegaMD.Filled = true;// Отмечаем, что палитра задана.
		// Палитра должна содержать не менее двух цветов: первый (прозрачный) и цвет, который будет отображаться.
		if(idx_col < 2)
		{
			QMessageBox::critical(this, DlgCap_Error, tr("Палитра должна содержать не менее двух цветов."));

			return;// Объект файла создан на стеке и будет автоматически закрыт при выходе.
		}
		else
		{
			QMessageBox::information(this, tr("Загрузка палитры"), tr("Прочитано %1 цветов.").arg(idx_col));
		}
	}

	UI_UpdatePalette();// Обновляем изображение.
}

void MainWindow::on_btnCreatePalette_clicked()
{
/// \brief Модификация индексов исходной палитры. Для цвета с меньшим индексом ничего не меняется, для цвета с большим индексом устанавлиется ссылка на цвет с меньшим индексом.
/// При этом следует исправить индексы на целевы цвета для всех исходных цветов после изменённого.
auto SrcPalette_ModIdx = [&](const size_t pIndex1, const size_t pIndex2) -> void
{
size_t idx_const = (pIndex1 < pIndex2 ? pIndex1 : pIndex2);
size_t idx_mod = (pIndex1 > pIndex2 ? pIndex1 : pIndex2);

	for(size_t idx_start = 0, idx_e = mSrcImageData.Palette_SegaMD.size(); idx_start < idx_e; idx_start++)
	{
		if(mSrcImageData.Palette_SegaMD[idx_start].TgtPal_ColIdx == idx_mod)
			mSrcImageData.Palette_SegaMD[idx_start].TgtPal_ColIdx = idx_const;
		else if(mSrcImageData.Palette_SegaMD[idx_start].TgtPal_ColIdx > idx_mod)
			mSrcImageData.Palette_SegaMD[idx_start].TgtPal_ColIdx--;
	}
};

	mPalette_SegaMD.Clear();
	// Проверим палитру исходного изображения: если количество не превышает допустимого, то можно использоавть готовую палитру.
	if((size_t)mSrcImageData.Palette_SegaMD.size() <= SPalette_SegaMD::Color_MaxCount)
	{
		for(size_t idx = 0, idx_e = (size_t)mSrcImageData.Palette_SegaMD.size(); idx < idx_e; idx++)
			mPalette_SegaMD.ARGB[idx] = mSrcImageData.Palette_SegaMD[idx].TgtARGB;

		mPalette_SegaMD.Filled = true;// Отмечаем, что палитра задана.
	}
	else
	{// Если цветов больше чем может уместиться в палитре, то необходимо объединить самые близкие друг к другу цвета.
		struct SPalette_Temp
		{
			QRgb Color;
			size_t Count;
		};

		QVector<SPalette_Temp> pal_temp;
		size_t distance;
		size_t idxsave_col1 = 0, idxsave_col2 = 0;
		size_t tval;

		// Для сокращения палитры воспользуемся промежуточной палитрой, которую и будем сокращать.
		// Задаём начальные значения индексов.
		pal_temp.resize(mSrcImageData.Palette_SegaMD.size());
		for(size_t idx = 0, idx_e = mSrcImageData.Palette_SegaMD.size(); idx < idx_e; idx++)
		{
			pal_temp[idx].Color = mSrcImageData.Palette_SegaMD[idx].TgtARGB;
			pal_temp[idx].Count = mSrcImageData.Palette_SegaMD[idx].Count;
			mSrcImageData.Palette_SegaMD[idx].TgtPal_ColIdx = idx;
		}

		// Цикл сокращения палитры.
		do
		{
			distance = SIZE_MAX;
			for(size_t idx_col1 = 0, idx_col1_e = pal_temp.size() - 2; idx_col1 < idx_col1_e; idx_col1++)
			{
				for(size_t idx_col2 = idx_col1 + 1, idx_col2_e = pal_temp.size() - 1; idx_col2 < idx_col2_e; idx_col2++)
				{
					tval = ColorDistance(pal_temp[idx_col1].Color, pal_temp[idx_col2].Color);
					if(tval < distance)
					{
						distance = tval;
						idxsave_col1 = idx_col1;
						idxsave_col2 = idx_col2;
					}
				}
			}// for(size_t idx_col1 = 0, idx_col1_e = color_arr.size() - 2; idx_col1 < idx_col1_e; idx_col1++)

#if 0
			{// Рассчитываем цвет, которым можно заменить оба исходных цвета. При рассчёте учитываем количество точек, которые используют исходные цвета.
				const size_t pixcnt1 = mSrcImageData.Palette[idxsave_col1].Count;
				const size_t pixcnt2 = mSrcImageData.Palette[idxsave_col2].Count;
				const size_t pixcnt_all = mSrcImageData.Palette[idxsave_col1].Count + mSrcImageData.Palette[idxsave_col2].Count;

				// Находим средний цвет между найденными с минимальным расстоянием.
				color_arr[idxsave_col1] = qRgb(
												(pixcnt1 * qRed(color_arr[idxsave_col1]) + pixcnt2 * qRed(color_arr[idxsave_col2])) / pixcnt_all,
												(pixcnt1 * qGreen(color_arr[idxsave_col1]) + pixcnt2 * qGreen(color_arr[idxsave_col2])) / pixcnt_all,
												(pixcnt1 * qBlue(color_arr[idxsave_col1]) + pixcnt2 * qBlue(color_arr[idxsave_col2])) / pixcnt_all
											);
			}
#else
			if(pal_temp[idxsave_col1].Count < pal_temp[idxsave_col2].Count)
				pal_temp[idxsave_col1] = pal_temp[idxsave_col2];
#endif
			// В исходно палитре устанавливаем соответствие целевым цветам палитры.
			SrcPalette_ModIdx(idxsave_col1, idxsave_col2);
			// В сокращаемой палитре удаляем лишний цвет.
			pal_temp.remove(idxsave_col2);
		} while((size_t)pal_temp.size() > SPalette_SegaMD::Color_MaxCount);

		// Копируем данные в целевую палитру.
		for(size_t idx = 0, idx_e = pal_temp.size(); idx < idx_e; idx++)
			mPalette_SegaMD.ARGB[idx] = pal_temp[idx].Color;

		mPalette_SegaMD.Filled = true;// Отмечаем, что палитра задана.
	}// if((size_t)mSrcImageData.Palette.size() <= SPalette_SegaMD::Color_MaxCount) else

	UI_UpdatePalette();
}

void MainWindow::on_btnCreateSpriteSheet_clicked(bool pChecked)
{
	if(pChecked)
	{
		// Первое, что мы проверяем - существование палитры.
		if(!mPalette_SegaMD.Filled)
		{
			QMessageBox::critical(this, tr("Палитра не задана"), tr("Необходимо открыть или создать палитру."));
			ui->btnCreateSpriteSheet->setChecked(false);

			return;
		}

		BuildMode_Enable();
	}
	else
	{
		BuildMode_Cancel();
	}
}
