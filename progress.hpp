#pragma once

#include <QDialog>

namespace Ui {
class Progress;
}

class Progress : public QDialog
{
	Q_OBJECT

private:

	Ui::Progress *ui;

public:

	explicit Progress(QWidget *pParent);
	~Progress();

	/// \brief Устанавливает заголовок окна и вызывает обработку событий.
	void SetTitle(const QString& pTitle);

	/// \brief Устанавливает процент выполнения операции и вызывает обработку событий.
	void SetProgress(const size_t pPercent);
};
