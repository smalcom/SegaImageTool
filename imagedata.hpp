#pragma once

// Заголовчные файлы Qt.
#include <QImage>

/// \brief Точка на плоскости.
struct SPoint
{
	size_t X, Y;

	SPoint() {}
	SPoint(const size_t pX, const size_t pY) : X(pX), Y(pY) {}
};

/// \brief Прямоугольник на плоскости.
struct SRect
{
	SPoint TopLeft, BottomRight;

	SRect() {}
	SRect(const SPoint pTopLeft, const SPoint pBottomRight) : TopLeft(pTopLeft), BottomRight(pBottomRight) {}

	/// \return true - если точка \ref pPoint лежит внутри прямоугольника или на его сторонах.
	bool Contains(const SPoint pPoint);
};

/// \brief Исходные данные, связанные с изображением.
struct SSrcImageData
{
	struct SPalette
	{
		QRgb TgtARGB;///< Цвет в целевом формате.
		size_t Count;///< Количество ссылок на цвет: сколько точек исходного изображения используют этот цвет.
		/// \var TgtPal_ColIdx
		/// Индекс соответствующего цвета в целевой палитре. Используется если палитра исходного изображения слишком велика, чтобы использоваться на целевой платформе.
		size_t TgtPal_ColIdx;
	};

	QString FileName;///< имя файла с изображением.
	QImage Image;///< Исходное изображение.
	QVector<SPalette> Palette_SegaMD;///< Палитра исходного изображения в формате для целевой платформы.
	QVector<SRect> Contour;///< Прямоугольник, описывающий контур.
	QVector<size_t> ContourSelected;///< Массив выделенных контуров.
};

/// \brief Палитра для целевой платформы Sega MegaDrive/Genesys (далее SegaMD).
struct SPalette_SegaMD
{
	static constexpr size_t Color_MaxCount = 16;///< Количество цветов в палитре. Важно помнить, что первый цвет прозрачный.
	static constexpr size_t Color_Size = sizeof(uint16_t);///< Размер типа данных для хранения цвета.

	bool Filled;///< Флаг, обозначающий, что палитра задана.
	QRgb ARGB[Color_MaxCount];///< Цвета палитры в формате SegaMD. ( \sa QRGB_FromARGB).

	SPalette_SegaMD();

	/// \ref Обнуляет все цвета палитры.
	void Clear();

	/// \brief Преобразовывает цвет в формате ARGB в формат SegaMD: применяет маску для сокращения диапазона значений.
	static QRgb QRGB_FromARGB(const QRgb pARGB);

	/// \brief Преобразовывает цвет в формате SegaMD в формат ARGB: добавляет младшие биты, чтобы использовать максимальную яркость.
	static QRgb QRGB_ExpTo_ARGB(const QRgb pSegaARGB);
};

/// \brief Рассчитывает отличие (дистанцию) между цветами с учётом цветового восприятия.
size_t ColorDistance(const QRgb pColor1, const QRgb pColor2);
