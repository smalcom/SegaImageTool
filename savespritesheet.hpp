#pragma once

// Заголовочные файлы Qt.
#include <QDialog>
#include <QMap>

// Заголовочные файлы проекта.
#include "imagedata.hpp"

namespace Ui {
class SaveSpriteSheet;
}

class SaveSpriteSheet : public QDialog
{
	Q_OBJECT

private:

	/// \brief Данные для построения предпросмотротра и целевого изображения.
	struct STgtImageData
	{
		struct SImageData
		{
			QImage* Image;///< Выбранное изображение, приведённое к размеру, соответствующему целевому формату.
			QPixmap Pixmap;///< Картинка для предпросмотра.
		};

		QMap<QRgb, size_t> ColorMap;///< Карта соответствий между цветами оригинального изображения и цветами из палитры.
		QVector<SImageData> ImageData;///< Изображение.
		QVector<QRgb> ColorTable;///< Цветовая палитра в в формате, необходимом для \ref Image.
	};

	const SSrcImageData& mSrcImageData;
	const SPalette_SegaMD& mTgtPalette;

	Ui::SaveSpriteSheet *ui;
	STgtImageData mTgtImageData;

public:

	explicit SaveSpriteSheet(QWidget *pParent, const SSrcImageData& pSrcImageData, const SPalette_SegaMD& pTgtPalette);
	~SaveSpriteSheet();

	/// \brief Возвращает результат выбора пользователя.
	/// \param [in] pImageIdx - индекс изображения.
	/// \return изображение.
	const QImage* Result_Image(const size_t pImageIdx) const;

	/// \brief Возвращает результат выбора пользователя.
	/// \return количество изображений.
	size_t Result_ImageCount() const;

	/// \brief Возвращает результат выбора пользователя.
	/// \return база имён файлов.
	QString Result_NameBase() const;

	/// \brief Возвращает результат выбора пользователя.
	/// \return true - если выбран порядок плиток "Спрайт", false - если выбран порядок плиток "Карта".
	bool Result_TileOrder_Sprite() const;

	/// \brief Возвращает результат выбора пользователя.
	/// \return true - если выбрано объединение файлов с плитками, иначе - false.
	bool Result_UniteFiles() const;
};
